#!/bin/bash
set -e

gitlab-runner unregister --all-runners

# Register GitLab Runner
gitlab-runner register --non-interactive \
	--registration-token "${GITLAB_SHARED_RUNNERS_REGISTRATION_TOKEN?}" \
	--template-config /template-config.toml \
	--name "runner-docker" \
	--executor "docker"

# Calls the GitLab runner vendor entrypoint to not override the default behavior.
/entrypoint "$@"
